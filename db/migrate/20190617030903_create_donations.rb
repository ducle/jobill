class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.integer :campaign_id
      t.integer :user_id
      t.decimal :amount
      t.string :transaction_id
      t.text :note
      t.string :stripe_card_id
      t.string :stripe_card_last4
      t.boolean :charged, default: false

      t.timestamps
    end
    add_index :donations, :campaign_id
    add_index :donations, :user_id
  end
end
