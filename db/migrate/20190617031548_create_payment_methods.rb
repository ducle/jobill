class CreatePaymentMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_methods do |t|
      t.integer :user_id
      t.string :stripe_card_id
      t.string :stripe_card_last4
      t.integer :stripe_exp_month
      t.integer :stripe_exp_year
      t.boolean :is_default, default: false

      t.timestamps
    end
    add_index :payment_methods, :user_id
    add_index :payment_methods, :stripe_card_id
  end
end
