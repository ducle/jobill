class CreateCampaigns < ActiveRecord::Migration[5.2]
  def change
    create_table :campaigns do |t|
      t.integer :user_id
      t.string :title
      t.text :description
      t.decimal :goal_amount
      t.decimal :raised_amount
      t.string :slug

      t.timestamps
    end
    add_index :campaigns, :user_id
    add_index :campaigns, :slug
  end
end
