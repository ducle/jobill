# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create({
  first_name: "Steve",
  last_name: "JOB",
  email: "job@apple.com",
  password: "password",
})

user1 = User.create({
  first_name: "Steve",
  last_name: "Balmer",
  email: "balmer@apple.com",
  password: "password",
})

user.campaigns.create({
  title: "OxyGenesis Institute",
  description: "" "
    Please help us make growing up MUCH easier, by empowering kids (and their adults!) to quickly turn anxiety, self-doubt and pain into mindfulness, confidence and self-esteem. For the full scoop, click on lower right corner of video (above) to view it full screen.
    USE OF FUNDS: 100% of your non-profit donation (minus 3% merchant fee if you pay online) will be used to:
    1) Implement the marketing plan so we can:
    a) Recruit 2 schools for our no-cost pilot program
    b) Round-up 10 child-friendly corporations to sponsor future programs
    2) Hire a part-time administrative assistant and two independent contractors as required to develop the systems and resources needed for long-term sustainable growth of our mission.
    3) Create the Mobile App/Game - already been developed on paper.
    4) Revise the original book and print more copies.
    WANT MORE DETAILS? See update below for links to evidence-based studies about EFT/Tapping and Belly Button Breathing.
    For school program details, visit www.MavensMagicButtons.com/educators
    TIMING: This fundraising campaign ends on December 31, 2018, so please donate as much as you can now.
    *OxyGenesis Institute is a fiscally-sponsored project of Sponsor, Inc., a 501c3 non-profit based in Framingham, MA.
    PLEASE SHARE this page with friends and family via email, social media or conversation. Thank you!
  " "",
  goal_amount: 20_000,
})
