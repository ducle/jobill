stripeTokenHandler = (formId, token)->
  form = document.getElementById(formId)
  $('#stripe-card-token').val(token.id)

  console.log(token);
  form.submit()
card = null
needCard = true 
bindingDonation = (stripe, formId, eleId)->
  elements = stripe.elements()
  card = elements.create('card', {style: {base: {fontSize: '16px', color: "#32325d"}}});
  card.mount("##{eleId}");
  card.addEventListener 'change', (event)->
    if (event.error)
      $('.card-errors').show().text(event.error.message)
    else
      $('.card-errors').hide()
      $("##{formId}").removeAttr('disabled')
      $("##{formId}").find('[type="submit"]').removeAttr('disabled')
  $("##{formId}").on 'submit', (event)->
    if needCard
      event.preventDefault()
      stripe.createToken(card).then (result)->
        console.log('card', result)
        if (result.error)
          $('.card-errors').show().text(result.error.message)
        else 
          stripeTokenHandler(formId, result.token)
          $('.card-errors').hide()
          $("##{formId}").removeAttr('disabled')
          $("##{formId}").find('[type="submit"]').removeAttr('disabled')

bindingPayment = (stripe, formId, eleId)->
  bindingDonation(stripe, formId, eleId)
  $ ()-> 
    $('[name="donation[stripe_card_id]"]').on 'change', ()-> 
      console.log('ddddd', $(this).val())
      value = $(this).val()
      if(value == '' || !value)
        needCard = false 
        $(".stripe-card-group").show()
        bindingDonation(stripe, formId, eleId)
      else 
        needCard = false 
        $(".stripe-card-group").hide()
        $('.card-errors').hide()
        $("##{eleId}").val('')
        $("##{formId}").removeAttr('disabled')
        $("##{formId}").find('[type="submit"]').removeAttr('disabled')        
        if card 
          card.destroy()
window.bindingDonation = bindingPayment
# window.bindingDonation = bindingDonation