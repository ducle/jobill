class DonationsController < ApplicationController
  before_action :authenticate_user!

  def new
    @campaign = Campaign.friendly.find(params[:campaign_id])
    # @default_payment = current_user.default_payment
    # puts @default_payment.inspect
    # @donation = Donation.new(stripe_card_id: @default_payment.try(:stripe_card_id))
    @donation = Donation.new()
  end

  def create
    @campaign = Campaign.friendly.find(params[:campaign_id])
    @donation = @campaign.donations.new(donation_params)
    @donation.user_id = current_user.id
    if @donation.save
      message = "Thank you for your donation!"
      if @donation.errors.any?
        message = @donation.errors.full_messages.join(", ")
      end
      redirect_to @campaign, notice: message
    else
      render :new
    end
  end

  def confirm
    unless request.post?
      redirect_to campaign_path(params[:campaign_id])
    else
      @campaign = Campaign.find(params[:campaign_id])
      @donation = @campaign.donations.new(donation_params)
    end
  end

  def show
    @donation = Donation.find(params[:id])
  end

  private

  def donation_params
    params.require(:donation).permit(
      :amount,
      :stripe_card_token,
      :stripe_card_id,
      :note
    )
  end
end
