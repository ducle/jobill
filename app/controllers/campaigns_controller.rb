class CampaignsController < ApplicationController
  def index
  end

  def show
    @campaign = Campaign.friendly.find(params[:id])
  end
end
