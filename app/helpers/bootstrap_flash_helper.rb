module BootstrapFlashHelper
  ALERT_TYPES = [:success, :info, :warning, :danger] unless const_defined?(:ALERT_TYPES)

  def bootstrap_flash(options = {})
    flash_messages = []
    flash.each do |type, message|
      next if message.blank?

      type = type.to_sym
      type = :success if type == :notice
      type = :danger  if type == :alert
      type = :danger  if type == :error
      next unless ALERT_TYPES.include?(type)

      tag_class = options.extract!(:class)[:class]
      tag_options = {
        class: "alert alert-#{type} #{tag_class} text-center"
      }.merge(options)

      close_button = content_tag(:button, raw("&times;"), type: "button", class: "close", "data-dismiss" => "alert")

      Array(message).each do |msg|
        text = content_tag(:div, close_button + raw(msg), tag_options)
        flash_messages << text if msg
      end
    end
    flash_messages.join("\n").html_safe
  end

  def current_path? path_name
    path = request.path
    if path_name.class.name == 'Regexp' 
      if path =~ path_name
        return 'active'
      else
        return ''
      end
    end
    if path_name.split('#').include?(controller_name) &&  path_name.split('#').include?(action_name)
      return 'active'
    end
    if path =~ Regexp.new(path_name.gsub(':id/', '\d{1,10}/'), 'i')
      return 'active'
    end
    # if path =~ Regexp.new(path_name.gsub(':id/', '\d{1,10}/'), 'i') || path_name =~ Regexp.new(controller_name, 'i') || path_name =~ Regexp.new(action_name, 'i')
    #   return 'active'
    # end
  end
end
