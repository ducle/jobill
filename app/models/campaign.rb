class Campaign < ApplicationRecord
  extend FriendlyId
  belongs_to :user
  has_many :donations

  friendly_id :title, use: :slugged

  validates :title, presence: true
  validates :goal_amount, presence: true, numericality: { greater_than: 0 }

  def raised_total
    @raised_total ||= donations.where(charged: true).sum(:amount)
  end

  def percentage
    raised_total.to_f * 100 / [goal_amount.to_f, 1].max
  end
end
