class Donation < ApplicationRecord
  belongs_to :campaign
  belongs_to :user

  validates :amount, presence: true, numericality: { greater_than: 1 }

  attr_accessor :stripe_card_token

  before_create :purchasing_amount_on_stripe

  def donator_name
    user.name
  end

  private

  def purchasing_amount_on_stripe
    if user && campaign
      card_id = stripe_card_id
      begin
        if !stripe_card_token.blank?
          card_id = user.add_stripe_token!(stripe_card_token)
        end
        charge = Stripe::Charge.create({
          amount: (amount.to_f * 100).to_i,
          customer: user.stripe_customer_id,
          source: card_id,
          currency: "usd",
          description: "Fund on campaign##{campaign.id} #{campaign.title}",
        })
        self.transaction_id = charge.id
        self.charged = true
      rescue Exception => ex
        Rails.logger.error("Could not charge donation #{ex.message}")
        self.errors.add(:base, ex.message)
        return false
      end
    end
  end
end
