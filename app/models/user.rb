class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :campaigns
  has_many :donations
  has_many :payment_methods

  validates :first_name, presence: true
  validates :last_name, presence: true

  def name
    if !first_name.blank? || !last_name.blank?
      [first_name, last_name].compact.join(" ")
    else
      email
    end
  end

  def default_payment
    payment_methods.find_by(is_default: true)
  end

  def stripe_customer
    return nil if stripe_customer_id.blank?
    begin
      @stripe_customer ||= Stripe::Customer.retrieve(stripe_customer_id)
    rescue => ex
      Rails.logger.error(ex.message)
      return nil
    end
  end

  def add_stripe_token!(token)
    customer = stripe_customer
    if customer
      card = customer.sources.create(source: token)
      customer.default_source = card.id
      customer.save
    else
      customer = Stripe::Customer.create(
        source: token,
        email: email,
        description: "Customer for #{email}",
      )
      self.update_attribute(:stripe_customer_id, customer.id)
      card = customer.sources.first
    end
    payment_method = self.payment_methods.find_or_initialize_by(stripe_card_id: card.id)
    payment_method.stripe_card_last4 = card.last4
    payment_method.stripe_exp_month = card.exp_month
    payment_method.stripe_exp_year = card.exp_year
    payment_method.is_default = true
    payment_method.save!
    self.payment_methods.where.not(id: payment_method.id).update_all(is_default: false)
    return card.id
  end
end

# t.string :stripe_card_id
# t.string :stripe_card_last4
# t.integer :stripe_exp_month
# t.integer :stripe_exp_year
# t.boolean :is_default, default: false
