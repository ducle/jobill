class PaymentMethod < ApplicationRecord
  belongs_to :user
  validates :stripe_card_id, presence: true, uniqueness: true
end
